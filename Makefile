# This MUST come before any other Makefile inclues. The smartest thing
# is likely to have it be the first line in the file
SRC_DIR:=$(shell dirname $(realpath $(firstword $(MAKEFILE_LIST))))/firmware

APP_TARGET?=Nexus

HOME_DIR?=$(HOME)
PARTICLE_TOOLCHAIN=$(HOME_DIR)/.particle/toolchains
VISUAL_CODE_DIR=$(HOME_DIR)/.vscode

include $(PARTICLE_TOOLCHAIN)/buildscripts/1.8.0/Makefile

DEVICE_OS_PATH=$(PARTICLE_TOOLCHAIN)/deviceOS/1.4.4/firmware-1.4.4/

export PATH := $(VISUAL_CODE_DIR)/extensions/particle.particle-vscode-core-1.8.1/src/cli/bin/darwin/amd64:$(PARTICLE_TOOLCHAIN)/gcc-arm/5.3.1/bin:$(PARTICLE_TOOLCHAIN)/buildtools/1.1.1:$(PARTICLE_TOOLCHAIN)/openocd/0.11.2-adhoc6ea4372.0/bin:$(PATH)

APPDIR=$(SRC_DIR)/$(APP_TARGET)
include $(APPDIR)/target-info.mk

# Compile the user app
build: target
	make compile-user APPDIR=$(APPDIR) DEVICE_OS_PATH=$(DEVICE_OS_PATH) PLATFORM=$(PLATFORM)

# Compile the user app for debugging
build_debug: target
	make compile-debug APPDIR=$(APPDIR) DEVICE_OS_PATH=$(DEVICE_OS_PATH) PLATFORM=$(PLATFORM)

# Get rid of compiled target files
clean:
	make clean-user APPDIR=$(APPDIR) DEVICE_OS_PATH=$(DEVICE_OS_PATH) PLATFORM=$(PLATFORM)

clean_debug:
	make clean-debug APPDIR=$(APPDIR) DEVICE_OS_PATH=$(DEVICE_OS_PATH) PLATFORM=$(PLATFORM)

# Get rid of all compiled files
clean_all:
	make sanitize APPDIR=$(APPDIR) DEVICE_OS_PATH=$(DEVICE_OS_PATH) PLATFORM=$(PLATFORM)

# Compile and flash the user app
flash: build
	stty -f $(PORT) 14400
	make flash-user APPDIR=$(APPDIR) DEVICE_OS_PATH=$(DEVICE_OS_PATH) PLATFORM=$(PLATFORM)

# Compile and flash the user debug build
flash_debug: build
	stty -f $(PORT) 14400
	make flash-debug APPDIR=$(APPDIR) DEVICE_OS_PATH=$(DEVICE_OS_PATH) PLATFORM=$(PLATFORM)

generate_git_header:
	python $(SRC_DIR)/generate_git_header.py --input_file=$(SRC_DIR)/Libraries/gitcommit.template.h --output_file=$(SRC_DIR)/$(APP_TARGET)/gitcommit.h

target: generate_git_header
	@echo "****************************$(HOME_DIR)**************************"

