#ifndef HOLDING_TANK_H
#define HOLDING_TANK_H

#include "Libraries/AfloatAbstractNode.h"

class HoldingTank : public AfloatAbstractNode {
public:
   HoldingTank(String sensorName, String dataFieldName, String description, uint32_t sampleInterval_ms, uint32_t connectionInterval_s, uint16_t maxNumPendingAcks);

   void identityHandler(const char *event, const char *jsonMsgPacket);
   void publishData(time_t measurementTime, float tankMeasurement_L, uint32_t numberOfRetries = 0);
   void republishData(const payload_t &payloadData, uint32_t numberOfRetries = 0);

};



#endif  // HOLDING_TANK_H
